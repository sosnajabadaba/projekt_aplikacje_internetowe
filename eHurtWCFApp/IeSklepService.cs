﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace eHurtWCFApp
{
    [ServiceContract]
    public interface IeSklepService
    {
        //towary
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/Towary")]
        List<eSklep.Towar> GetTowary();

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/Towary")]
        eSklep.Towar PostTowary(eSklep.Towar t);

        [OperationContract]
        [WebInvoke(Method = "PUT",
            UriTemplate = "/Towary")]
        bool PutTowary(eSklep.Towar t);

        [OperationContract]
        [WebInvoke(Method = "DELETE",
            UriTemplate = "/Towary/{id}")]
        bool DeleteTowary(string id);

        //kontrahent
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/Kontrahenci")]
        List<eSklep.Kontrahent> GetKontrahenci();

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/Kontrahenci")]
        eSklep.Kontrahent PostKontrahenci(eSklep.Kontrahent t);

        [OperationContract]
        [WebInvoke(Method = "PUT",
            UriTemplate = "/Kontrahenci")]
        bool PutKontrahenci(eSklep.Kontrahent t);

        [OperationContract]
        [WebInvoke(Method = "DELETE",
            UriTemplate = "/Kontrahenci/{id}")]
        bool DeleteKontrahenci(string id);

        //zamowienia nagłówek
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/Zamowienia/Kontrahent/{id}")]
        List<eSklep.ZamowienieGroup> GetZamowienia(string id);

        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/Zamowienia")]
        List<eSklep.ZamowienieGroup> GetZamowieniaW();

        [OperationContract]
        [WebInvoke(Method = "PUT",
            UriTemplate = "/Zamowienia")]
        bool PutZamowienia(eSklep.ZamowienieGroup g);

        //inne
        [OperationContract]
        [WebInvoke(Method = "PUT",
            UriTemplate = "/Path")]
        string GetPath();
    }

    namespace eSklep
    {
        public class Towar
        {
            public int Id { get; set; }
            public string Nazwa { get; set; }
            public decimal CenaNetto { get; set; }
            public int Vat { get; set; }
        }

        public class Kontrahent
        {
            public int Id { get; set; }
            public string Nazwa { get; set; }
            public string Imie { get; set; }
            public string Nazwisko { get; set; }
            public string Adres { get; set; }
            public string Nip { get; set; }
        }

        public class ZamowienieGroup
        {
            public ZamowienieNag Zamowienie { get; set; }

            public List<ZamowieniePoz> Pozycje { get; set; }
        }

        public class ZamowienieNag
        {
            public int Id { get; set; }
            public int KontrahentId { get; set; }
            public string Status { get; set; }
            public string DataZamowienia { get; set; }
        }

        public class ZamowieniePoz
        {
            public int Id { get; set; }
            public int ZamNagId { get; set; }
            public int TowarId { get; set; }
            public decimal Ilosc { get; set; }
            public decimal CenaNetto { get; set; }
            public int Vat { get; set; }
        }

        public class ZamowienieStat
        {
            public string Id { get; set; }
            public string Nazwa { get; set; }
        }
    }
}
