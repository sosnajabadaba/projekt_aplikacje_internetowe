﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;

namespace eHurtWCFApp
{
    public enum EnumService { NONE, EHURT, EHURTTEST, PAYUP, EFRESH }; 

    static public class Config
    {
        public static bool DEBUG = false;

        static string connStr = @"Server=10.65.9.203;Database=ehurt;uid=sa;pwd=bpr4422.2013";

        static string connStrTest = @"Server=10.65.9.203;Database=echurtownia;uid=sa;pwd=bpr4422.2013";

        static string connStrPortal = @"Server=10.65.9.200;Database=portalec;uid=sa;pwd=bpr4422.2013";


        static public string GetConnStrCheck(out EnumService service)
        {
            string url = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri.OriginalString;

            if (url.Contains("ehurttestservice") || url.Contains("ekierowcatestservice"))
            {
                service = EnumService.EHURTTEST;

                return connStrTest;
            }
            else if (url.Contains("payupservice"))
            {
                service = EnumService.PAYUP;

                return connStrPortal;
            }
            else if (url.Contains("efresh"))
            {
                service = EnumService.EFRESH;

                return connStrPortal;
            }
            else
            {
                service = EnumService.EHURT;

                return connStr;
            }
        }

        static public string GetConnStr()
        {
            EnumService service;

            return GetConnStrCheck(out service);
        }

        static public bool CheckUser(User user)
        {
            try
            {
                EnumService service;

                string connStr = GetConnStrCheck(out service);

                if (service == EnumService.PAYUP)
                {
                    return Membership.ValidateUser(user.Login, user.Password);
                }
                else if(service == EnumService.EHURT || service == EnumService.EHURTTEST)
                {
                    using (SqlConnection conn = new SqlConnection(connStr))
                    {
                        string query = @"select UzytkownikId from Uzytkownicy where Login = @login and Haslo = @haslo and Status != 'U'";

                        using (SqlCommand cmd = new SqlCommand(query, conn))
                        {
                            SqlParameter param = new SqlParameter("@login", user.Login);
                            SqlParameter param2 = new SqlParameter("@haslo", user.Password);

                            cmd.Parameters.AddRange(new SqlParameter[] { param, param2 });

                            conn.Open();

                            using (SqlDataReader rdr = cmd.ExecuteReader())
                            {
                                if (rdr.Read())
                                {
                                    return true;
                                }

                                return false;
                            }
                        }
                    }
                }
                else if (service == EnumService.EFRESH)
                {
                    using (SqlConnection conn = new SqlConnection(connStr))
                    {
                        string query = @"select UserId from Users where Login = @login and Haslo = @haslo";

                        using (SqlCommand cmd = new SqlCommand(query, conn))
                        {
                            SqlParameter param = new SqlParameter("@login", user.Login);
                            SqlParameter param2 = new SqlParameter("@haslo", user.Password);

                            cmd.Parameters.AddRange(new SqlParameter[] { param, param2 });

                            conn.Open();

                            using (SqlDataReader rdr = cmd.ExecuteReader())
                            {
                                if (rdr.Read())
                                {
                                    return true;
                                }

                                return false;
                            }
                        }
                    }
                }
                else
                {
                    Exceptions.ThrowException("Not implemented", System.Net.HttpStatusCode.NotImplemented);

                    return false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message, System.Net.HttpStatusCode.InternalServerError);

                return false;
            }
        }
    }
}