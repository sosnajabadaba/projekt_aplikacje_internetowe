﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Net;

namespace eHurtWCFApp
{   
    public class AuthorizationManager : ServiceAuthorizationManager
    {
        public override bool CheckAccess(OperationContext operationContext)
        {
            string authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];

            if ((authHeader != null) && (authHeader != string.Empty))
            {
                string[] svcCredentials = System.Text.ASCIIEncoding.ASCII
                            .GetString(Convert.FromBase64String(authHeader.Substring(6)))
                            .Split(':');

                User user = new User{ Login = svcCredentials[0], Password = svcCredentials[1] };

                if (Config.CheckUser(user))
                {
                    return true;
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;

                    throw new WebFaultException<string>("Błędny login lub hasło", HttpStatusCode.Unauthorized);
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"mobi sync\"");
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;

                throw new WebFaultException<string>("Proszę podać login i hasło", HttpStatusCode.Unauthorized);
            }
        }

        static public User GetActualUser()
        {
            string authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];

            if ((authHeader != null) && (authHeader != string.Empty))
            {
                string[] svcCredentials = System.Text.ASCIIEncoding.ASCII
                            .GetString(Convert.FromBase64String(authHeader.Substring(6)))
                            .Split(':');

                return new User { Login = svcCredentials[0], Password = svcCredentials[1] };
            }
            else
            {
                return null;
            }
        }

        static public User GetDebugUser(string login, string password)
        {
            return new User() { Login = login, Password = password };
        }
    }
}