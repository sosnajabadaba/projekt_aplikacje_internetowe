﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHurtWCFApp
{
    public class CustomHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PostRequestHandlerExecute += (s, e) =>
                {
                    HttpResponse response = context.Response;

                    if (response != null)
                    {
                        if (response.Filter != null)
                        {
                            response.Filter = new System.IO.Compression.GZipStream(response.Filter, System.IO.Compression.CompressionMode.Compress);
                            response.AppendHeader("Content-Encoding", "gzip");
                        }
                    }
                };
        }

        public void Dispose()
        {

        }
    }
}