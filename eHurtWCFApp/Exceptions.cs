﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Web;

namespace eHurtWCFApp
{
    static public class Exceptions
    {
        static public void ThrowException(string msg)
        {
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;

            response.StatusCode = System.Net.HttpStatusCode.Unused;
            response.StatusDescription = msg;

            throw new WebFaultException<string>(msg, System.Net.HttpStatusCode.Unused);
        }

        static public void ThrowException(string msg, System.Net.HttpStatusCode code)
        {
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;

            response.StatusCode = code;
            response.StatusDescription = msg;

            throw new WebFaultException<string>(msg, code);
        }
    }
}