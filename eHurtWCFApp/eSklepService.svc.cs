﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Data.SQLite;
using System.Web;
using System.Threading;

namespace eHurtWCFApp
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class eSklepService : IeSklepService
    {
        string connStr = @"data source=" + HttpContext.Current.Server.MapPath(".") + @"\trak\baza";

        public string GetPath()
        {
            return connStr;
        }

        //towary
        public List<eSklep.Towar> GetTowary()
        {
            List<eSklep.Towar> list = new List<eSklep.Towar>();

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query = "select Id, Nazwa, CenaNetto, Vat from Towary";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        conn.Open();

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                eSklep.Towar t = new eSklep.Towar();

                                t.Id = rdr.GetInt32(0);
                                t.Nazwa = rdr.GetString(1);
                                t.CenaNetto = rdr.GetDecimal(2);
                                t.Vat = rdr.GetInt32(3);

                                list.Add(t);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return list;
        }

        public eSklep.Towar PostTowary(eSklep.Towar t)
        {
            ValidateTowar(t);

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query1 = "update Towary set Nazwa = @nazwa, CenaNetto = @cena, Vat = @vat where Id = @id";

                    string query = "select Id, Nazwa, CenaNetto, Vat from Towary where Id = @id";

                    using (SQLiteCommand cmd = new SQLiteCommand(query1, conn))
                    {
                        cmd.Parameters.AddRange(new SQLiteParameter[] 
                            {
                                new SQLiteParameter("@nazwa", t.Nazwa),
                                new SQLiteParameter("@cena", t.CenaNetto.ToString().Replace(",", ".")),
                                new SQLiteParameter("@vat", t.Vat),
                                new SQLiteParameter("@id", t.Id)
                            });

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = query;

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                t = new eSklep.Towar();

                                t.Id = rdr.GetInt32(0);
                                t.Nazwa = rdr.GetString(1);
                                t.CenaNetto = rdr.GetDecimal(2);
                                t.Vat = rdr.GetInt32(3);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return t;
        }

        public bool PutTowary(eSklep.Towar t)
        {
            ValidateTowar(t);

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query1 = "insert into Towary(Nazwa, CenaNetto, Vat) values(@nazwa, @cena, @vat)";

                    using (SQLiteCommand cmd = new SQLiteCommand(query1, conn))
                    {
                        cmd.Parameters.AddRange(new SQLiteParameter[] 
                            {
                                new SQLiteParameter("@nazwa", t.Nazwa),
                                new SQLiteParameter("@cena", t.CenaNetto.ToString().Replace(",", ".")),
                                new SQLiteParameter("@vat", t.Vat)
                            });

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return false;
        }

        public bool DeleteTowary(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                Exceptions.ThrowException("Wartość parametru @id nie może być pusta");
            }

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query = "delete from Towary where Id = @id";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SQLiteParameter("@id", id));

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return false;
        }

        private void ValidateTowar(eSklep.Towar t)
        {
            if (String.IsNullOrEmpty(t.Nazwa))
            {
                Exceptions.ThrowException("Brak nazwy towaru");
            }

            if (t.CenaNetto <= 0)
            {
                Exceptions.ThrowException("Cena towaru musi być większa od zera");
            }

            if (t.Vat < 0)
            {
                Exceptions.ThrowException("Vat nie może być wartością mniejszą od zera");
            }
        }
        
        //kontrahenci
        public List<eSklep.Kontrahent> GetKontrahenci()
        {
            List<eSklep.Kontrahent> list = new List<eSklep.Kontrahent>();

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query = "select Id, coalesce(Nazwa, ''), Imie, Nazwisko, Adres, coalesce(Nip, '') from Kontrahent";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        conn.Open();

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                eSklep.Kontrahent t = new eSklep.Kontrahent();

                                t.Id = rdr.GetInt32(0);
                                t.Nazwa = rdr.GetString(1);
                                t.Imie = rdr.GetString(2);
                                t.Nazwisko = rdr.GetString(3);
                                t.Adres = rdr.GetString(4);
                                t.Nip = rdr.GetString(5);
                                
                                list.Add(t);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return list;
        }

        public eSklep.Kontrahent PostKontrahenci(eSklep.Kontrahent t)
        {
            ValidateKontrahent(t);

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query1 = "update Kontrahent set Nazwa = @nazwa, Imie = @imie, Nazwisko = @nazwisko, Adres = @adres, Nip = @nip where Id = @id";

                    string query = "select Id, coalesce(Nazwa, ''), Imie, Nazwisko, Adres, coalesce(Nip, '') from Kontrahent where Id = @id";

                    using (SQLiteCommand cmd = new SQLiteCommand(query1, conn))
                    {
                        cmd.Parameters.AddRange(new SQLiteParameter[] 
                            {
                                new SQLiteParameter("@nazwa", t.Nazwa),
                                new SQLiteParameter("@imie", t.Imie),
                                new SQLiteParameter("@nazwisko", t.Nazwisko),
                                new SQLiteParameter("@adres", t.Adres),
                                new SQLiteParameter("@nip", t.Nip),
                                new SQLiteParameter("@id", t.Id)
                            });

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = query;

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                t = new eSklep.Kontrahent();

                                t.Id = rdr.GetInt32(0);
                                t.Nazwa = rdr.GetString(1);
                                t.Imie = rdr.GetString(2);
                                t.Nazwisko = rdr.GetString(3);
                                t.Adres = rdr.GetString(4);
                                t.Nip = rdr.GetString(5);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return t;
        }

        public bool PutKontrahenci(eSklep.Kontrahent t)
        {
            ValidateKontrahent(t);

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query1 = "insert into Kontrahent(Nazwa, Imie, Nazwisko, Adres, Nip) values(@nazwa, @imie, @nazwisko, @adres, @nip)";

                    using (SQLiteCommand cmd = new SQLiteCommand(query1, conn))
                    {
                        cmd.Parameters.AddRange(new SQLiteParameter[] 
                            {
                                new SQLiteParameter("@nazwa", t.Nazwa),
                                new SQLiteParameter("@imie", t.Imie),
                                new SQLiteParameter("@nazwisko", t.Nazwisko),
                                new SQLiteParameter("@adres", t.Adres),
                                new SQLiteParameter("@nip", t.Nip)
                            });

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return false;
        }

        public bool DeleteKontrahenci(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                Exceptions.ThrowException("Wartość parametru @id nie może być pusta");
            }

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query = "delete from Kontrahent where Id = @id";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SQLiteParameter("@id", id));

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return false;
        }

        private void ValidateKontrahent(eSklep.Kontrahent t)
        {
            if (String.IsNullOrEmpty(t.Imie))
            {
                Exceptions.ThrowException("Brak imienia kontrahenta");
            }

            if (String.IsNullOrEmpty(t.Nazwisko))
            {
                Exceptions.ThrowException("Brak nazwiska kontrahenta");
            }

            if (String.IsNullOrEmpty(t.Adres))
            {
                Exceptions.ThrowException("Brak adresu kontrahenta");
            }
        }

        public List<eSklep.ZamowienieGroup> GetZamowienia(string id)
        {
            List<eSklep.ZamowienieGroup> list = new List<eSklep.ZamowienieGroup>();

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query = @"select n.Id, n.KontrahentId, n.Status, n.DataZamowienia,
                                        p.Id, p.ZamNagId, p.TowarId, p.Ilosc, p.CenaNetto, p.Vat
                                    from ZamowienieNag n
                                    join ZamowieniePoz p on p.ZamNagId = n.Id
                                    where n.KontrahentId = @id
                                    order by n.Id";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SQLiteParameter("@id", id));

                        conn.Open();

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            int lastid = 0;

                            eSklep.ZamowienieGroup g = null;
                            eSklep.ZamowienieNag n = null;

                            while (rdr.Read())
                            {
                                int zamid = rdr.GetInt32(0);

                                if (zamid != lastid)
                                {
                                    lastid = zamid;

                                    g = new eSklep.ZamowienieGroup();

                                    n = new eSklep.ZamowienieNag();

                                    n.Id = zamid;
                                    n.KontrahentId = rdr.GetInt32(1);
                                    n.Status = rdr.GetString(2);
                                    n.DataZamowienia = rdr[3].ToString();

                                    g.Zamowienie = n;
                                    g.Pozycje = new List<eSklep.ZamowieniePoz>();
                                        
                                    list.Add(g);
                                }

                                eSklep.ZamowieniePoz p = new eSklep.ZamowieniePoz();

                                p.Id = rdr.GetInt32(4);
                                p.ZamNagId = zamid;
                                p.TowarId = rdr.GetInt32(6);
                                p.Ilosc = rdr.GetDecimal(7);
                                p.CenaNetto = rdr.GetDecimal(8);
                                p.Vat = rdr.GetInt32(9);

                                g.Pozycje.Add(p);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return list;
        }

        public List<eSklep.ZamowienieGroup> GetZamowieniaW()
        {
            List<eSklep.ZamowienieGroup> list = new List<eSklep.ZamowienieGroup>();

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connStr))
                {
                    string query = @"select n.Id, n.KontrahentId, n.Status, n.DataZamowienia,
                                        p.Id, p.ZamNagId, p.TowarId, p.Ilosc, p.CenaNetto, p.Vat
                                    from ZamowienieNag n
                                    join ZamowieniePoz p on p.ZamNagId = n.Id
                                    order by n.Id";

                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                    {
                        conn.Open();

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            int lastid = 0;

                            eSklep.ZamowienieGroup g = null;
                            eSklep.ZamowienieNag n = null;

                            while (rdr.Read())
                            {
                                int zamid = rdr.GetInt32(0);

                                if (zamid != lastid)
                                {
                                    lastid = zamid;

                                    g = new eSklep.ZamowienieGroup();

                                    n = new eSklep.ZamowienieNag();

                                    n.Id = zamid;
                                    n.KontrahentId = rdr.GetInt32(1);
                                    n.Status = rdr.GetString(2);
                                    n.DataZamowienia = rdr[3].ToString();

                                    g.Zamowienie = n;
                                    g.Pozycje = new List<eSklep.ZamowieniePoz>();

                                    list.Add(g);
                                }

                                eSklep.ZamowieniePoz p = new eSklep.ZamowieniePoz();

                                p.Id = rdr.GetInt32(4);
                                p.ZamNagId = zamid;
                                p.TowarId = rdr.GetInt32(6);
                                p.Ilosc = rdr.GetDecimal(7);
                                p.CenaNetto = rdr.GetDecimal(8);
                                p.Vat = rdr.GetInt32(9);

                                g.Pozycje.Add(p);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ThrowException(ex.Message);
            }

            return list;
        }

        public bool PutZamowienia(eSklep.ZamowienieGroup g)
{
    ValidateZam(g);

    try
    {
        using (SQLiteConnection conn = new SQLiteConnection(connStr))
        {
            string query = @"insert into ZamowienieNag(KontrahentId, Status, DataZamowienia) values (@kontrahentid, 'N', datetime('now', 'localtime'))";

            string query2 = "select max(Id) from ZamowienieNag";

            string query3 = @"insert into ZamowieniePoz(ZamNagId, TowarId, Ilosc, CenaNetto, Vat)
                                values(@zamnagid, @towarid, @ilosc, @cena, @vat)";

            conn.Open();

            using (SQLiteTransaction trans = conn.BeginTransaction())
            {
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(query, conn, trans))
                    {
                        cmd.Parameters.Add(new SQLiteParameter("@kontrahentid", g.Zamowienie.KontrahentId));

                        cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();

                        cmd.CommandText = query2;

                        int zamnagid = 0;

                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            if (rdr.Read())
                            {
                                zamnagid = rdr.GetInt32(0);
                            }
                            else
                            {
                                throw new Exception("Nie znaleziono nagłówka zamówienia");
                            }
                        }

                        cmd.CommandText = query3;

                        SQLiteParameter param = new SQLiteParameter("@zamnagid", zamnagid);
                        SQLiteParameter param2 = new SQLiteParameter("@towarid");
                        SQLiteParameter param3 = new SQLiteParameter("@ilosc");
                        SQLiteParameter param4 = new SQLiteParameter("@cena");
                        SQLiteParameter param5 = new SQLiteParameter("@vat");

                        cmd.Parameters.AddRange(new SQLiteParameter[]
                            {
                                param,
                                param2,
                                param3,
                                param4,
                                param5
                            });

                        g.Pozycje.ForEach(p =>
                            {
                                param2.Value = p.TowarId;
                                param3.Value = p.Ilosc.ToString().Replace(",", ".");
                                param4.Value = p.CenaNetto.ToString().Replace(",", ".");
                                param5.Value = p.Vat;

                                cmd.ExecuteNonQuery();
                            });

                        trans.Commit();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw ex;
                }
            }
        }
    }
    catch (Exception ex)
    {
        Exceptions.ThrowException(ex.Message);
    }

    return false;
}

        public void ValidateZam(eSklep.ZamowienieGroup g)
        {
            if (g == null)
            {
                Exceptions.ThrowException("Brak obiektu grupującego zamówienia");
            }

            if (g.Zamowienie == null)
            {
                Exceptions.ThrowException("Brak obiektu nagłówka zamówienia");
            }

            if (g.Zamowienie.KontrahentId <= 0)
            {
                Exceptions.ThrowException("Brak identyfikatora kontrahenta");
            }

            if (g.Pozycje == null || g.Pozycje.Count() == 0)
            {
                Exceptions.ThrowException("Brak pozycji zamówienia");
            }

            g.Pozycje.ForEach(p =>
                {
                    if (p.TowarId <= 0)
                    {
                        Exceptions.ThrowException("Brak identyfikatora towaru");
                    }

                    if (p.CenaNetto <= 0)
                    {
                        Exceptions.ThrowException("Cena towaru musi być większa niż zero");
                    }

                    if (p.Ilosc <= 0)
                    {
                        Exceptions.ThrowException("Ilość zamawianego towaru musi być większa od zero");
                    }
                });
        }
    }
}
